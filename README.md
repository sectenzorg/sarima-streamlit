# SARIMA Streamlit Docker Build and Run Guide

## Overview
This guide provides instructions for building and running the SARIMA (Seasonal Autoregressive Integrated Moving Average) model visualization using Streamlit within a Docker container.

## Building the Docker Image

### Prerequisites
Make sure you have Docker installed on your system. If not, you can download it from [Docker's official website](https://www.docker.com/get-started).

### Building the Docker Image
1. Clone the repository containing the Dockerfile for SARIMA Streamlit.
2. Navigate to the directory containing the Dockerfile.

### Build Command
Use the following command to build the Docker image:

```bash
docker build -f .docker/Dockerfile -t sectenzorg/sarima-streamlit .
```

This command will build the Docker image named `sectenzorg/sarima-streamlit` using the Dockerfile located at `.docker/Dockerfile`.

## Running the Docker Container

### Starting the Container
After successfully building the Docker image, you can run the SARIMA Streamlit application with the following command:

```bash
docker run -dit --name sarima-streamlit -p 8501:8501 sectenzorg/sarima-streamlit
```

### Accessing the Application
Once the container is running, access the SARIMA Streamlit application by opening your web browser and navigating to:

```
http://localhost:8501
```

If you are using Docker Toolbox or Docker Machine, replace `localhost` with the IP address of your Docker host.

## Notes
- Ensure that port `8501` is available and not used by another application on your host machine.
- Modify Dockerfile path (`-f .docker/Dockerfile`) if your Dockerfile is located in a different directory.

---
