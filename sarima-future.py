import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pmdarima import auto_arima
from statsmodels.tsa.arima.model import ARIMA
from sklearn.metrics import mean_squared_error, mean_absolute_error
from datetime import datetime
import os

# Function to run Streamlit app
os.system("streamlit run D:\\02_Prototype Tesis\\tesis\\sarimaFutureStreamlit.py")

# Title and description
st.title('Aplikasi Prediksi Harga Saham')
st.write("Ini adalah aplikasi untuk memprediksi harga saham menggunakan SARIMA")

# File uploader for the CSV file
uploaded_file = st.file_uploader("Upload File CSV", type=["csv"])

if uploaded_file is not None:
    # Load the data
    data = pd.read_csv(uploaded_file)

    # Extract the base file name without extension for output file naming
    base_filename = uploaded_file.name.split('.')[0]
    current_date = datetime.now().strftime('%Y%m%d')

    # Convert 'Date' column to datetime type if it's not already
    data['Date'] = pd.to_datetime(data['Date'])

    # Split the data into train and test sets
    train_size = int(len(data) * 0.9)
    train_data, test_data = data[:train_size], data[train_size:]

    # Use 'Open', 'High', 'Low', 'Adj Close', 'Volume' as features to predict 'Close'
    features = ['Open', 'High', 'Low', 'Adj Close', 'Volume']

    # Input for number of future periods to predict
    future_periods = st.number_input('Masukan Jumlah Hari Yang Akan Di Prediksi', min_value=1, max_value=365, value=30)

    # Execute button
    if st.button('Eksekusi SARIMA Model'):
        with st.spinner('Menjalankan model SARIMA...'):
            # Get optimal parameters from auto_arima
            model_auto = auto_arima(train_data['Close'], exogenous=train_data[features], start_p=1, start_q=1,
                                    max_p=3, max_q=3, m=12, start_P=0, seasonal=True, d=1, D=1,
                                    trace=True, error_action='ignore', suppress_warnings=True, stepwise=True)

            order = model_auto.get_params()['order']
            seasonal_order = model_auto.get_params()['seasonal_order']

            # Fit the ARIMA model
            model = ARIMA(train_data['Close'], exog=train_data[features], order=order)
            fit_model = model.fit()

            # Forecast using the trained model
            forecast = fit_model.forecast(steps=len(test_data), exog=test_data[features])

            # Calculate evaluation metrics: RMSE, MAE, MAPE
            rmse = np.sqrt(mean_squared_error(test_data['Close'], forecast))
            mae = mean_absolute_error(test_data['Close'], forecast)
            mape = np.mean(np.abs((test_data['Close'] - forecast) / test_data['Close'])) * 100

            # Calculate the number of training and testing data points
            num_train_data = len(train_data)
            num_test_data = len(test_data)
            total_data = num_train_data + num_test_data

            # Calculate percentage of training and testing data
            percent_train_data = round((num_train_data / total_data) * 100)
            percent_test_data = round((num_test_data / total_data) * 100)

            # Save the forecast results to a DataFrame
            result = pd.DataFrame({'Date': test_data['Date'], 'Actual Close': test_data['Close'], 'Predicted Close': forecast})

            # Future predictions
            future_exog = data[features].tail(future_periods).values  # Assuming the features for future periods are available
            future_forecast = fit_model.forecast(steps=future_periods, exog=future_exog)
            future_dates = pd.date_range(start=test_data['Date'].iloc[-1] + pd.Timedelta(days=1), periods=future_periods)

            # Save the future forecast results to a DataFrame
            future_result = pd.DataFrame({'Date': future_dates, 'Predicted Close': future_forecast})

            # Combine the dates and forecasts for plotting
            combined_dates = pd.concat([test_data['Date'], pd.Series(future_dates)])
            combined_forecast = np.concatenate([forecast, future_forecast])

            # Display information about the data split
            st.subheader('Parameter Terbaik')
            st.write(f"Best ARIMA parameters: {order}")
            st.write(f"Best seasonal parameters: {seasonal_order}")

            st.subheader('Split Data')
            st.write(f"Jumlah Data: {total_data}")
            st.write(f"Jumlah Data Training: {num_train_data} ({percent_train_data}%)")
            st.write(f"Jumlah Data Testing: {num_test_data} ({percent_test_data}%)")

            st.subheader('Evaluation Metrics')
            # Display evaluation metrics
            st.write(f"Root Mean Squared Error (RMSE): {rmse:.6f}")
            st.write(f"Mean Absolute Error (MAE): {mae:.6f}")
            st.write(f"Mean Absolute Percentage Error (MAPE): {mape:.6f}%")

            # Save the prediction results to an Excel file
            result_file_name = f'report/{base_filename}_hasilsarima_{current_date}.xlsx'
            future_result_file_name = f'report/{base_filename}_hasilsarima_future_{current_date}.xlsx'
            result.to_excel(result_file_name, index=False)
            future_result.to_excel(future_result_file_name, index=False)

            st.subheader('Detail Prediksi vs Aktual:')
            st.write(result)
            st.write(f"Hasil prediksi berhasil disimpan ke dalam file {result_file_name}")

            st.subheader('Prediksi Saham:')
            st.write(future_result)
            st.write(f"Hasil prediksi saham di masa depan berhasil disimpan ke dalam file {future_result_file_name}")

            st.subheader('SARIMA Training, Testing, Test Prediction & Future Forecast')
            # Plotting
            fig, ax = plt.subplots(figsize=(12, 6))
            ax.plot(train_data['Date'], train_data['Close'], label='Training', color='blue')
            ax.plot(test_data['Date'], test_data['Close'], label='Testing', color='green')
            ax.plot(test_data['Date'], forecast, label='Test Prediction', linestyle='--', color='orange')
            ax.plot(future_dates, future_forecast, label='Future Forecast', linestyle='--', color='red')
            ax.set_ylabel('Close Price')
            ax.set_title('SARIMA Training, Testing, Test Prediction & Future Forecast')
            ax.legend()
            ax.xaxis.set_major_locator(plt.MaxNLocator(12))
            st.pyplot(fig)

            st.subheader('SARIMA - Testing, Test Prediction & Future Forecast')
            fig, ax = plt.subplots(figsize=(12, 6))
            ax.plot(test_data['Date'], test_data['Close'], label='Testing', color='green')
            ax.plot(test_data['Date'], forecast, label='Test Prediction', linestyle='--', color='orange')
            ax.plot(future_dates, future_forecast, label='Future Forecast', linestyle='--', color='red')
            ax.set_ylabel('Close Price')
            ax.set_title('SARIMA - Testing, Test Prediction & Future Forecast')
            ax.legend()
            ax.xaxis.set_major_locator(plt.MaxNLocator(12))
            st.pyplot(fig)

            st.subheader('SARIMA - Training, Test Prediction & Future Forecast')
            fig, ax = plt.subplots(figsize=(12, 6))
            ax.plot(train_data['Date'], train_data['Close'], label='Training', color='blue')
            ax.plot(test_data['Date'], forecast, label='Test Prediction', linestyle='--', color='orange')
            ax.plot(future_dates, future_forecast, label='Future Forecast', linestyle='--', color='red')
            ax.set_ylabel('Close Price')
            ax.set_title('SARIMA - Training, Test Prediction & Future Forecast')
            ax.legend()
            ax.xaxis.set_major_locator(plt.MaxNLocator(12))
            st.pyplot(fig)

            st.subheader('SARIMA - Training Data')
            fig, ax = plt.subplots(figsize=(12, 6))
            ax.plot(train_data['Date'], train_data['Close'], label='Training', color='blue')
            ax.set_ylabel('Close Price')
            ax.set_title('SARIMA - Training Data')
            ax.legend()
            ax.xaxis.set_major_locator(plt.MaxNLocator(12))
            st.pyplot(fig)
